package com.sourceit

class User(
    var name: String,
    var age: Int = 18,
    var email: String = ""
) {

    companion object {
        const val CONSTANT = 100

        fun a() {

        }
    }

    fun printInfo() {
        println("$name, $age, $email")
    }
}

