package com.sourceit

data class DataUser(
    var name: String,
    var age: Int = 18,
    var email: String = ""
)