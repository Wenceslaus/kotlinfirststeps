package com.sourceit

const val CONSTANT = 1

fun main(args: Array<String>) {
    val user = User("Vasya", 19, "mail@mail.com")
    user.name = "Fedya"

    val userNameOnly = User("Vasya", email = "mail@mail.com")

    val dataUser = DataUser("")
    dataUser.isAdult()

    val admin = Admin("Admin")
    admin.name = "Fedya"
    println(admin.name)
}