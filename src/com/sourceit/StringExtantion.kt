package com.sourceit

fun String.getFirstLetter(): String {
    return this[0].toString()
}